﻿using AspNetAsync.Models;
using System.Collections.Generic;

namespace AspNetAsync.Controllers
{
    public class BestProductsViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public int TotalProductsCount { get; set; }
    }
}