﻿using AspNetAsync.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AspNetAsync.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BestProducts()
        {
            using (var context = new AdventureContext())
            {
                List<Product> products = context.Products.Take(5).ToList();
                int totalProducts = context.Products.Count();

                return View(new BestProductsViewModel()
                {
                    Products = products,
                    TotalProductsCount = totalProducts
                });
            }
        }

        public async Task<ActionResult> BestProductsAsync()
        {
            using (var context = new AdventureContext())
            {
                List<Product> products = await context.Products.Take(5).ToListAsync();
                int totalProducts = await context.Products.CountAsync();

                return View("BestProducts", new BestProductsViewModel()
                {
                    Products = products,
                    TotalProductsCount = totalProducts
                });
            }
        }

        public async Task<ActionResult> BestProductsConcurrentQueriesAsync()
        {
            using (var context = new AdventureContext())
            {
                Task<List<Product>> products = context.Products.Take(5).ToListAsync();
                Task<int> totalProducts = context.Products.CountAsync();
                await Task.WhenAll(products, totalProducts);

                return View("BestProducts", new BestProductsViewModel()
                {
                    Products = products.Result,
                    TotalProductsCount = totalProducts.Result
                });
            }
        }

        public async Task<ActionResult> BestProductsConcurrentReadersAsync()
        {
            string connectionString = ConfigurationManager.ConnectionStrings
                ["Default"].ConnectionString;

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var productsCommand = new SqlCommand("SELECT TOP 5 ProductID, Name, ProductNumber FROM Production.Product") { Connection = connection };
                var productsCountCommand = new SqlCommand("SELECT COUNT(*) FROM Production.Product") { Connection = connection };

                Task<List<Product>> productsTask = productsCommand.ExecuteReaderAsync().ContinueWith(t =>
                {
                    var products = new List<Product>(5);
                    using (SqlDataReader reader = t.Result)
                    {
                        while (reader.Read())
                        {
                            products.Add(new Product()
                            {
                                ProductID = (int)reader["ProductID"],
                                Name = (string)reader["Name"],
                                ProductNumber = (string)reader["ProductNumber"]
                            });
                        }
                    }

                    return products;
                });

                Task<int> productsCountTask = productsCountCommand.ExecuteScalarAsync().ContinueWith(t => (int)t.Result);

                await Task.WhenAll(productsTask, productsCountTask);
                return View("BestProducts", new BestProductsViewModel()
                {
                    Products = productsTask.Result,
                    TotalProductsCount = productsCountTask.Result
                });
            }
        }

        public Task<ActionResult> BestProductsManualAsync()
        {
            var taskCompletionSource = new TaskCompletionSource<ActionResult>();

            var context = new AdventureContext();

            context.Products.Take(5).ToListAsync()
                .ContinueWith(productListTask =>
                {
                    context.Products.CountAsync()
                     .ContinueWith(productsCount =>
                     {
                         context.Dispose();
                         taskCompletionSource.SetResult(
                             View("BestProducts", new BestProductsViewModel()
                             {
                                 Products = productListTask.Result,
                                 TotalProductsCount = productsCount.Result
                             }));
                     });
                });

            return taskCompletionSource.Task;
        }

        public ActionResult BestProductsWithWaitAsync()
        {
            BestProductsViewModel viewModel = BuildViewModelAsync().Result;
            return View("BestProducts", viewModel);
        }

        public async Task<BestProductsViewModel> BuildViewModelAsync()
        {
            using (var context = new AdventureContext())
            {
                List<Product> products = await context.Products.Take(5)
                    .ToListAsync()
                    .ConfigureAwait(false);

                int totalProducts = await context.Products
                    .CountAsync()
                    .ConfigureAwait(false);

                return new BestProductsViewModel()
                {
                    Products = products,
                    TotalProductsCount = totalProducts
                };
            }
        }

        [AsyncTimeout(100)]
        public async Task<ActionResult> BestProductsCancellableAsync(CancellationToken cancellationToken)
        {
            using (var context = new AdventureContext())
            {
                List<Product> products = await context.Products.Take(5).ToListAsync(cancellationToken);
                int totalProducts = await context.Products.CountAsync(cancellationToken);

                return View("BestProducts", new BestProductsViewModel()
                {
                    Products = products,
                    TotalProductsCount = totalProducts
                });
            }
        }

    }
}