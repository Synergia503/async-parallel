﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSample
{
    public partial class BestProductReport : Form
    {
        private readonly string _connectionString = "Data Source=.;Initial Catalog=AdventureWorks2014;Integrated Security=True";
        private readonly string _connectionString2 = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AdventureWorks2016;Integrated Security=True";
        // Not recommended
        //private SynchronizationContext _synchronizationContext;

        // Bad solution
        //DataTable newThreadResult = null;

        public BestProductReport()
        {
            InitializeComponent();
        }

        private void btnRaport_Click(object sender, EventArgs e)
        {
            using (var connection = CreateOpenConnection())
            {
                SqlCommand command = CreateReportCommand(connection);

                SqlDataReader resultReader = command.ExecuteReader();
                resultGrid.DataSource = LoadDataIntoTable(resultReader);
            }
        }

        private static DataTable LoadDataIntoTable(SqlDataReader resultReader)
        {
            var resultTable = new DataTable();
            resultTable.Load(resultReader);
            return resultTable;
        }

        private static SqlCommand CreateReportCommand(SqlConnection connection)
        {
            string queryContent = File.ReadAllText("./Report.sql");
            var command = new SqlCommand(queryContent) { Connection = connection };
            return command;
        }

        private SqlConnection CreateOpenConnection()
        {
            var connection = new SqlConnection(_connectionString2);
            connection.Open();
            return connection;
        }

        private void btnThread_Click(object sender, EventArgs e)
        {
            var synchronizationContext = SynchronizationContext.Current;
            Thread thread = new Thread(GenerateReportInNewThread);
            thread.Start(synchronizationContext);


            // Bad solution
            //var timer = new System.Windows.Forms.Timer() { Interval = 500 };
            //timer.Tick += (s, args) =>
            //{
            //    if (newThreadResult != null)
            //    {
            //        resultGrid.DataSource = newThreadResult;
            //        timer.Stop();
            //    }
            //};

            //timer.Start();
        }


        private void GenerateReportInNewThread(object synchronizationContext)
        {
            var synchContext = (SynchronizationContext)(synchronizationContext);
            using (var connection = CreateOpenConnection())
            {
                SqlCommand command = CreateReportCommand(connection);

                SqlDataReader resultReader = command.ExecuteReader();

                DataTable newThreadResult = LoadDataIntoTable(resultReader);

                // With SynchronizationContext
                synchContext.Post(dt =>
                {
                    resultGrid.DataSource = dt;
                }, newThreadResult);

                // With BeginInvoke
                //resultGrid.BeginInvoke((Action)(() =>
                //{
                //    resultGrid.DataSource = newThreadResult;
                //}));


                // Bad solution
                //newThreadResult = LoadDataIntoTable(resultReader);
            }
        }

        private void btnThreadPool_Click(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                CultureInfo defaultCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("pl-PL");
                using (var connection = CreateOpenConnection())
                {
                    SqlCommand command = CreateReportCommand(connection);

                    SqlDataReader resultReader = command.ExecuteReader();

                    DataTable newThreadResult = LoadDataIntoTable(resultReader);
                    resultGrid.BeginInvoke((Action)(() =>
                    {
                        resultGrid.DataSource = newThreadResult;
                    }));
                }

                Thread.CurrentThread.CurrentUICulture = defaultCulture;
            });
        }

        private void btnTask_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                using (var connection = CreateOpenConnection())
                {
                    SqlCommand command = CreateReportCommand(connection);

                    SqlDataReader resultReader = command.ExecuteReader();

                    DataTable newThreadResult = LoadDataIntoTable(resultReader);
                    resultGrid.BeginInvoke((Action)(() =>
                    {
                        resultGrid.DataSource = newThreadResult;
                    }));
                }
            });
        }

        private void btnBeginExecute_Click(object sender, EventArgs e)
        {
            SqlConnection connection = CreateOpenConnection();
            SqlCommand command = CreateReportCommand(connection);
            command.BeginExecuteReader(asyncResult =>
            {
                SqlDataReader dataReader = command.EndExecuteReader(asyncResult);
                DataTable dataTable = LoadDataIntoTable(dataReader);
                resultGrid.BeginInvoke((Action)(() =>
                {
                    resultGrid.DataSource = dataTable;
                }));

                connection.Dispose();
            }, null);

            // Bad code - it blocks UI thread
            //while (!asyncResult.IsCompleted)
            //{

            //}
        }

        private async void btnAsync_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = CreateOpenConnection())
            {
                SqlCommand command = CreateReportCommand(connection);
                SqlDataReader dataReader = await command.ExecuteReaderAsync();
                DataTable dataTable = LoadDataIntoTable(dataReader);
                resultGrid.BeginInvoke((Action)(() =>
                {
                    resultGrid.DataSource = dataTable;
                }));
            }
        }
    }
}