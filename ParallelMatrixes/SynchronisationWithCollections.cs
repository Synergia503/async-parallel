﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static System.Console;

namespace ParallelMatrixes
{
    public static class SynchronisationWithCollections
    {
        private const int _matrixCount = 50;
        private const int _matrixSize = 100;
        private static readonly Matrix m1 = MatrixGenerator.GenerateMatrix(_matrixSize);
        private static BlockingCollection<Matrix> _toCalculate = new BlockingCollection<Matrix>(new ConcurrentQueue<Matrix>());
        private static int _calculated = 0;
        private static ConcurrentDictionary<long, int> _timeHistogram = new ConcurrentDictionary<long, int>();
        private static ConcurrentBag<Matrix> _secondLevel = new ConcurrentBag<Matrix>();
        private static int _secondLevelCalculated = 0;

        public static void MultiplyMatrixes()
        {
            Task generator = Task.Run(GenerateMatrixesPeriodically);
            Task multiplicator1 = Task.Run((Action)Multiply);
            Task multiplicator2 = Task.Run((Action)Multiply);

            Task.WhenAll(generator, multiplicator1, multiplicator2)
                .Wait();

            foreach (KeyValuePair<long, int> time in _timeHistogram.OrderBy(t => t.Key))
            {
                WriteLine($"Time: {time.Key}\tValue: {time.Value}");
            }

            WriteLine($"Values count: {_timeHistogram.Values.Sum()}");
        }

        private static async Task GenerateMatrixesPeriodically()
        {
            for (int i = 0; i < _matrixCount; i++)
            {
                Matrix item = MatrixGenerator.GenerateMatrix(_matrixSize);
                _toCalculate.TryAdd(item);
            }
        }


        private static void PrintStatus(int resultedCount, long elapsedMilliseconds)
        {
            WriteLine(
                "Queue size: " + _toCalculate.Count.ToString().PadLeft(4)
                + "\tCalculated: " + resultedCount.ToString().PadLeft(4)
                + $"\tTime: {elapsedMilliseconds.ToString().PadLeft(4)}");
        }

        private static void Multiply()
        {
            while (_calculated < _matrixCount)
            {
                if (_toCalculate.TryTake(out Matrix m2, 300))
                {
                    var watch = new Stopwatch();
                    watch.Start();
                    Matrix result = m1.Multiply(m2);
                    watch.Stop();
                    int resultedCount = Interlocked.Increment(ref _calculated);
                    _secondLevel.Add(result);

                    _timeHistogram.AddOrUpdate(watch.ElapsedMilliseconds, 1, (k, v) => v + 1);

                    PrintStatus(resultedCount, watch.ElapsedMilliseconds);
                }
                else
                {
                    WriteLine("Failed to dequeue item");
                }
            }

            while (_secondLevelCalculated < _matrixCount)
            {
                if (_secondLevel.TryTake(out Matrix m2))
                {
                    m1.Multiply(m2);
                    int secondLevelResultedCount = Interlocked.Increment(ref _secondLevelCalculated);
                    WriteLine($"Second level calculated: {secondLevelResultedCount}");
                }
            }
        }
    }
}