﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using static System.Console;

namespace ParallelMatrixes
{
    public static class Synchronisation
    {
        private const int _matrixCount = 500;
        private const int _matrixSize = 100;
        private static readonly Matrix m1 = MatrixGenerator.GenerateMatrix(_matrixSize);
        private static Queue<Matrix> _toCalculate = new Queue<Matrix>();
        private static int _calculated = 0;
        private static object _lock = new object();
        private static Semaphore semaphore = new Semaphore(0, _matrixCount);
        private static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(0, _matrixCount);
        private static Mutex mutex = new Mutex();

        public static void MultiplyMatrixes()
        {
            Task generator = Task.Run(GenerateMatrixesPeriodicallyMutex);
            Task multiplicator1 = Task.Run((Action)MultiplyMutex);
            Task multiplicator2 = Task.Run((Action)MultiplyMutex);

            Task.WhenAll(generator, multiplicator1, multiplicator2)
                .Wait();
        }

        private static async Task GenerateMatrixesPeriodically()
        {
            for (int i = 0; i < _matrixCount; i++)
            {
                Matrix item = MatrixGenerator.GenerateMatrix(_matrixSize);

                //lock (_lock)
                //{  
                _toCalculate.Enqueue(item);
                //semaphore.Release();
                semaphoreSlim.Release();
                //}

                //await Task.Delay(100);
            }
        }

        private static async Task GenerateMatrixesPeriodicallyMutex()
        {
            for (int i = 0; i < _matrixCount; i++)
            {
                Matrix item = MatrixGenerator.GenerateMatrix(_matrixSize);

                //lock (_lock)
                //{  
                try
                {
                    mutex.WaitOne();
                    _toCalculate.Enqueue(item);
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }

        private static void PrintStatus()
        {
            WriteLine(
                "Queue size: " + _toCalculate.Count.ToString().PadLeft(4)
                + "\tCalculated: " + _calculated.ToString().PadLeft(4));
            WriteLine();
        }

        private static void Multiply()
        {
            while (_calculated < _matrixCount)
            {
                Matrix m2 = null;
                //if (_toCalculate.Count > 0)
                //{
                //    lock (_lock)
                //    {
                //        if (_toCalculate.Count > 0)
                //        {
                if (semaphoreSlim.Wait(100))
                //if (semaphore.WaitOne(100))
                {
                    m2 = _toCalculate.Dequeue();
                }
                //        }
                //    }
                //}

                if (m2 != null)
                {
                    m1.Multiply(m2);
                    Interlocked.Increment(ref _calculated);
                    PrintStatus();
                }
            }
        }

        private static void MultiplyMutex()
        {
            while (_calculated < _matrixCount)
            {
                Matrix m2 = null;
                if (_toCalculate.Count > 0)
                {
                    try
                    {
                        mutex.WaitOne();
                        if (_toCalculate.Count > 0)
                        {
                            m2 = _toCalculate.Dequeue();
                        }
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }

                if (m2 != null)
                {
                    m1.Multiply(m2);
                    Interlocked.Increment(ref _calculated);
                    PrintStatus();
                }
            }
        }
    }
}