﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ParallelMatrixes.Measurements;
using static System.Console;

namespace ParallelMatrixes
{
    internal static class Program
    {
        private static readonly int _matrixSize = 500;
        private static readonly Dictionary<string, TimeSpan> _processingTimes = new Dictionary<string, TimeSpan>();

        private static void Main(string[] args)
        {
            //ListOfMatrixesMultiplication();
            //TwoMatrixesMultiplication();
            //TimeSpan baseTime = Measure(() => Synchronisation.MultiplyMatrixes());
            //PrintSpeedup(baseTime, _processingTimes);
            SynchronisationWithCollections.MultiplyMatrixes();
        }

        private static void ListOfMatrixesMultiplication()
        {
            var _processingTimes = new Dictionary<string, TimeSpan>();
            Matrix m1 = MatrixGenerator.GenerateMatrix(_matrixSize);
            List<Matrix> matrixes = Enumerable.Range(0, 20)
                .Select(_ => MatrixGenerator.GenerateMatrix(_matrixSize))
                .ToList();

            TimeSpan baseTime = Measure(() => MultiplyAll(m1, matrixes));

            _processingTimes["Parallel Foreach"] = Measure(() => MultiplyAllParallel(m1, matrixes));

            _processingTimes["Parallel LINQ"] = Measure(() => MultiplyAllParallel(m1, matrixes));
            PrintSpeedup(baseTime, _processingTimes);
        }

        private static void TwoMatrixesMultiplication()
        {

            Matrix m1 = MatrixGenerator.GenerateMatrix(_matrixSize);
            Matrix m2 = MatrixGenerator.GenerateMatrix(_matrixSize);

            TimeSpan baseTime = Measure(() => m1.Multiply(m2));
            _processingTimes["Parallel Rows"] = Measure(() => m1.MultiplyParallelRows(m2));
            _processingTimes["Parallel Rows & Cols"] = Measure(() => m1.MultiplyParallelRowsAndCols(m2));
            _processingTimes["Parallel Cols inside non-parallel Rows"] = Measure(() => m1.MultiplyParallelCols(m2));
            _processingTimes["Parallel Rows manual with Tasks"] = Measure(() => m1.MultiplyParallelRowsManual(m2));
            _processingTimes["Parallel Rows manual with ThreadPool"] = Measure(() => m1.MultiplyParallelRowManualThreadPool(m2));

            PrintSpeedup(baseTime, _processingTimes);
        }

        private static void MultiplyAll(Matrix m1, List<Matrix> matrixes)
        {
            foreach (Matrix m2 in matrixes)
            {
                m1.Multiply(m2);
            }
        }

        private static void MultiplyAllParallel(Matrix m1, List<Matrix> matrixes)
        {
            Parallel.ForEach(matrixes, m2 =>
            {
                m1.Multiply(m2);
            });
        }

        private static void MultiplyAllParallelLinq(Matrix m1, List<Matrix> matrixes)
        {
            matrixes.AsParallel()
                .Select(m2 => m1.Multiply(m2))
                .ToList();
        }

        private static void PrintSpeedup(TimeSpan baseTime, Dictionary<string, TimeSpan> processingTimes)
        {
            Write("Base time".PadRight(50));
            WriteLine(baseTime);
            Write("Calculations kind".PadRight(50));
            Write("Time".PadRight(40));
            WriteLine("Acceleration");

            foreach (KeyValuePair<string, TimeSpan> actionWithMeasure in processingTimes)
            {
                double speedupScale = baseTime.TotalMilliseconds / actionWithMeasure.Value.TotalMilliseconds;
                Write(actionWithMeasure.Key.PadRight(50));
                Write(actionWithMeasure.Value.ToString().PadRight(40));
                WriteLine(speedupScale);
            }
        }
    }
}