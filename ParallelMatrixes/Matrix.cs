﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelMatrixes
{
    public class Matrix
    {
        public int[][] Values; // First = row, second = column
        public int Rows { get; }
        public int Cols { get; }

        public Matrix(int[][] values)
        {
            Values = values;
            Rows = values.Length;
            Cols = values[0].Length;
        }

        public Matrix Multiply(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            for (int i = 0; i < Rows; i++)
            {
                resultMatrix[i] = new int[other.Cols];

                for (int j = 0; j < other.Cols; j++)
                {
                    int result = 0;
                    for (int k = 0; k < Cols; k++)
                    {
                        result += Values[i][k] * other.Values[k][j];
                    }

                    resultMatrix[i][j] = result;
                }
            }

            return new Matrix(resultMatrix);
        }

        public Matrix MultiplyParallelRows(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            Parallel.For(0, Rows, i =>
            {
                resultMatrix[i] = new int[other.Cols];

                for (int j = 0; j < other.Cols; j++)
                {
                    int result = 0;
                    for (int k = 0; k < Cols; k++)
                    {
                        result += Values[i][k] * other.Values[k][j];
                    }

                    resultMatrix[i][j] = result;
                }
            });

            return new Matrix(resultMatrix);
        }

        public Matrix MultiplyParallelRowsAndCols(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            Parallel.For(0, Rows, i =>
            {
                resultMatrix[i] = new int[other.Cols];
                Parallel.For(0, other.Cols, j =>
                {
                    int result = 0;
                    for (int k = 0; k < Cols; k++)
                    {
                        result += Values[i][k] * other.Values[k][j];
                    }

                    resultMatrix[i][j] = result;
                });
            });

            return new Matrix(resultMatrix);
        }

        public Matrix MultiplyParallelCols(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            for (int i = 0; i < Rows; i++)
            {
                resultMatrix[i] = new int[other.Cols];

                Parallel.For(0, other.Cols, j =>
                {
                    int result = 0;
                    for (int k = 0; k < Cols; k++)
                    {
                        result += Values[i][k] * other.Values[k][j];
                    }

                    resultMatrix[i][j] = result;
                });
            }

            return new Matrix(resultMatrix);
        }

        public Matrix MultiplyParallelRowsManual(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            var tasks = new List<Task>(Rows);
            for (int i = 0; i < Rows; i++)
            {
                int counter = i;
                var task = Task.Run(() =>
                  {
                      resultMatrix[counter] = new int[other.Cols];

                      for (int j = 0; j < other.Cols; j++)
                      {
                          int result = 0;
                          for (int k = 0; k < Cols; k++)
                          {
                              result += Values[counter][k] * other.Values[k][j];
                          }

                          resultMatrix[counter][j] = result;
                      }
                  });

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

            return new Matrix(resultMatrix);
        }

        public Matrix MultiplyParallelRowManualThreadPool(Matrix other)
        {
            int[][] resultMatrix = new int[Rows][];
            int taskCount = 0;

            for (int i = 0; i < Rows; i++)
            {
                int counter = i;
                ThreadPool.QueueUserWorkItem(_ =>
                {
                    resultMatrix[counter] = new int[other.Cols];

                    for (int j = 0; j < other.Cols; j++)
                    {
                        int result = 0;
                        for (int k = 0; k < Cols; k++)
                        {
                            result += Values[counter][k] * other.Values[k][j];
                        }

                        resultMatrix[counter][j] = result;
                    }

                    Interlocked.Increment(ref taskCount);
                });
            }

            while (taskCount < Rows)
            {

            }

            return new Matrix(resultMatrix);
        }
    }
}